//
//  VedioManager.swift
//  YYVedioPlayer
//
//  Created by yuany on 2019/10/16.
//  Copyright © 2019 yuany. All rights reserved.
//

import Foundation

extension VedioManager {
    struct File: Hashable {
        let name: String
        let path: String
        let isFolder: Bool
    }
}

class VedioManager {
    static let dirDocument: URL = {
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return urls[urls.endIndex - 1]
    }()
    
    static let root = dirDocument.path
    
    static func load(at path: String) -> [File] {
        if let folder = try? YYFile().createFolderIfNeeded(at: path) {
            let folders = folder.subfolders.map {
                File(name: $0.name, path: $0.path, isFolder: true)
            }
            
            let files = folder.files.map {
                File(name: $0.name, path: $0.path, isFolder: false)
            }
            
            return folders + files
        }
        
        return []
    }
    
    static func delete(_ file: File) -> Bool {
        do {
            try FileManager.default.removeItem(atPath: file.path)
            return true
        } catch {
            print(error)
        }
        
        return false
    }
}
