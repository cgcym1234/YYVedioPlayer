//
//  PlayerViewController.swift
//  Swift-IJKPlayer
//
//  Created by 李利锋 on 2017/7/21.
//  Copyright © 2017年 leefeng. All rights reserved.
//

import UIKit
import IJKMediaFramework

protocol PlayerProtocol: class {
    func playerStartPlay()
    func playerStartPause()
    func playerStartComplete()
}

class PlayerViewController: UIViewController {
    private var player: IJKFFMoviePlayerController?
    private weak var videoPlayerView: VideoPlayerView?
    private var attendToPlay = false
    private var isMax = false
    
    var tapBackHandler: (()-> Void)?
    
    var height: CGFloat = 200
    var playerTitle: String?{
        didSet{
            videoPlayerView?.playerTitle = playerTitle
        }
    }
    
    func prepareFor(url: String, title: String) {
        DispatchQueue.main.async {
            self.url = url
            self.playerTitle = title
        }
    }
    
    private var url: String? {
        didSet{
            attendToPlay = false
            
            if let p = player,let v = videoPlayerView {
                p.shutdown()
                
                p.view.removeFromSuperview()
                v.removeAllObserver()
                v.removeFromSuperview()
            }
            
            let options = IJKFFOptions.byDefault()
            options?.setPlayerOptionIntValue(5, forKey: "framedrop")
            player = IJKFFMoviePlayerController(contentURLString: url, with: options)
            player?.scalingMode = .aspectFit
            player?.view.frame = view.frame
            player?.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            view.addSubview((player?.view)!)
            videoPlayerView = UINib(nibName: "VideoPlayerView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as? VideoPlayerView
            videoPlayerView?.frame = view.frame
            videoPlayerView?.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            videoPlayerView?.player = player
            videoPlayerView?.isMax(isMax)
            view.addSubview(videoPlayerView!)
            
            videoPlayerView?.mixOrMax = { [weak self] (isMax) in
                self?.rotateScreen(isMax: isMax)
            }
            
            videoPlayerView?.tapBackHandler = { [weak self] in
                self?.tapBackHandler?()
            }
            
            if isAutoPlay {
                player?.prepareToPlay()
            }
            
        }
    }
    
    var frame: CGRect? {
        didSet{
            guard let f = frame else { return  }
            view.frame = f
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    deinit {
        onDestory()
        print("PlayerViewController deinit")
    }
    
    //生命周期，必须要调用的
    private func onDestory() {
        //关闭播放器
        guard let p = player else { return  }
        p.shutdown()
        
        videoPlayerView?.removeAllObserver()
        player = nil
    }
    
    func rotateScreen(isMax: Bool) {
        print("旋转")
        self.isMax = isMax
        if (isMax) {//小屏->全屏
            UIView.animate(withDuration: 0.25, animations: {
                let value = UIInterfaceOrientation.landscapeRight.rawValue
                UIDevice.current.setValue(value, forKey: "orientation")
                self.view.frame = CGRect(x: 0, y: 0, width: max(UIScreen.main.bounds.width, UIScreen.main.bounds.height), height: min(UIScreen.main.bounds.width, UIScreen.main.bounds.height))
                self.player?.view.frame = (self.view.frame)
                self.videoPlayerView?.frame = (self.view.frame)
            })
        } else {//全屏->小屏
            UIView.animate(withDuration: 0.25, animations: {
                let value = UIInterfaceOrientation.portrait.rawValue
                UIDevice.current.setValue(value, forKey: "orientation")
                self.view.frame = CGRect(x: 0, y: 0, width: min(UIScreen.main.bounds.width, UIScreen.main.bounds.height), height: max(UIScreen.main.bounds.width, UIScreen.main.bounds.height))
                
                self.player?.view.frame = self.view.frame
                self.videoPlayerView?.frame = self.view.frame
            })
        }
    }
    
    ///获取封面ImageView
    func coverImageView() -> UIImageView? {
        return videoPlayerView?.coverImageView();
    }
    
    var isAutoPlay = true
    
    ///是否自动播放
    func isAutoPlay(autoPlay:Bool)  {
        self.isAutoPlay = autoPlay
    }
    ///播放器代理
    weak var playerProtocol:PlayerProtocol? {
        didSet{
            videoPlayerView?.playerProtocol = playerProtocol
        }
    }
    
    func play()  {
        videoPlayerView?.playOrPause(isPlay:true)
    }
    
    func pause()  {
        videoPlayerView?.playOrPause(isPlay:false)
    }
    
    func shutDown()  {
        player?.shutdown()
    }
}

extension PlayerViewController {
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        play()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        shutDown()
    }
}
