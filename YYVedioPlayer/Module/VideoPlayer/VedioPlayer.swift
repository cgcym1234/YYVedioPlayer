//
//  PlayerContainer.swift
//  YYVedioPlayer
//
//  Created by yuany on 2019/10/16.
//  Copyright © 2019 yuany. All rights reserved.
//

import SwiftUI

struct VedioPlayer {}

extension VedioPlayer {
    struct Container: View {
        let file: VedioManager.File
        
        var body: some View {
            /// For some reason, SwiftUI requires that you also set .navigationBarTitle for .navigationBarHidden to work properly.
            Content(url: file.path, title: file.name)
                .edgesIgnoringSafeArea(.all)
                .navigationBarTitle(Text(file.name), displayMode: .inline)
                .navigationBarHidden(true)
                .onDisappear { print("onDisappear") }
        }
    }
}

extension VedioPlayer {
    struct Content: UIViewControllerRepresentable {
        @Environment(\.presentationMode) var presentation
        
        let url: String
        let title: String
        
        func makeUIViewController(context: UIViewControllerRepresentableContext<Content>) -> PlayerViewController {
            let vc = PlayerViewController()
            vc.prepareFor(url: url, title: title)
            vc.tapBackHandler = {
                self.presentation.wrappedValue.dismiss()
            }
            
            return vc
        }
        
        func updateUIViewController(_ uiViewController: PlayerViewController, context: UIViewControllerRepresentableContext<Content>) {
            
        }
        
        func onAppear(perform action: (() -> Void)? = nil) -> some View {
            return self
        }
    }
}

