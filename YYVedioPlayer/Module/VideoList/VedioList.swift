//
//  ListView.swift
//  YYVedioPlayer
//
//  Created by yuany on 2019/10/16.
//  Copyright © 2019 yuany. All rights reserved.
//

import SwiftUI

struct VedioList {}

extension VedioList {
    struct Container: View {
        let path: String
        
        @State
        private var files: [VedioManager.File] = []
        //    @State var showRefreshView: Bool = false
        //    @State var pullStatus: CGFloat = 0
        
        var body: some View {
            /// 有bug
            //        RefreshableList(showRefreshView: $showRefreshView, pullStatus: $pullStatus, action: loadData) {
            //            VedioListView(files: self.files, delete: self.delete)
            //        }
            //        .navigationBarTitle(path.lastPathComponent)
            //        .onAppear(perform: loadData)
            
            Content(files: files, delete: delete)
                .navigationBarTitle(path.lastPathComponent)
                .onAppear(perform: loadData)
        }
        
        private func loadData() {
            print(path)
            let files = VedioManager.load(at: path)
            /// 不加DispatchQueue.main.async会crash
            DispatchQueue.main.async {
                self.files = files
            }
        }
        
        private func delete(at offsets: IndexSet) {
            for idx in offsets {
                let file = files[idx]
                if VedioManager.delete(file) {
                    files.remove(at: idx)
                }
            }
        }
    }
}

extension VedioList {
    struct Content: View {
        var files: [VedioManager.File]
        var delete: (_ offsets: IndexSet) -> Void
        
        var body: some View {
            List {
                ForEach(files, id: \.self) { file in
                    Group {
                        if file.isFolder {
                            NavigationLink(file.name, destination: Container(path: file.path))
                        } else {
                            NavigationLink(file.name, destination:VedioPlayer.Container(file: file))
                        }
                    }
                }
                .onDelete(perform: delete)
            }
        }
        
        private func cell(for file: VedioManager.File) -> AnyView {
            file.isFolder ?
                NavigationLink(file.name, destination: Container(path: file.path)).eraseToAnyView() :
                NavigationLink(file.name, destination:VedioPlayer.Container(file: file)).eraseToAnyView()
        }
    }
}


public extension View {
    func eraseToAnyView() -> AnyView {
        AnyView(self)
    }
}

