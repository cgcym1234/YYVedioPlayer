//
//  ContentView.swift
//  YYVedioPlayer
//
//  Created by yuany on 2020/1/17.
//  Copyright © 2020 yuany. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        NavigationView {
            VedioList.Container(path: VedioManager.root)
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}



